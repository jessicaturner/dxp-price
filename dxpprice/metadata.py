# -*- coding: utf-8 -*-
"""Project dxpprice

Information describing the project.
"""

# The package name, which is also the "UNIX name" for the project.
package = 'dxpprice'
project = "dxpprice"
project_no_spaces = project.replace(' ', '')
version = '0.4.2'
description = 'get price for DxpChain'
authors = ['Alt','Necklace']
authors_string = ', '.join(authors)
emails = ['pch957@163.com','xiangxn@163.com']
license = 'MIT'
copyright = '2015 ' + authors_string
url = 'https://github.com/xiangxn/dxpprice'
