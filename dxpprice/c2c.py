import json
import asyncio
import aiohttp
from bs4 import BeautifulSoup
from requests_html import AsyncHTMLSession, HTML


class C2C():
    def __init__(self):
        self.session = AsyncHTMLSession()
        # self.session.proxies = {"http": "socks5://127.0.0.1:1081", "https": "socks5://127.0.0.1:1081"}
        self.rate = {'CNY': {'CNY': 1.0}, 'USD': {'USD': 1.0}}

    async def _get_okex_price(self, currency="cny", _type="buy", symbol="btc"):
        url = "https://www.okex.com/p2p-markets/{}/{}-{}".format(currency, _type, symbol)
        # print(url)
        try:
            response = await asyncio.wait_for(self.session.get(url), 120)
            if response.status_code == 200:
                # print(response.html)
                await response.html.arender(sleep=2)
                # print(response.html.html)
                soup = BeautifulSoup(response.html.html, 'html.parser')
                # print(soup)
                list = soup.find_all("span", {"class": "price c-{}".format(_type)})
                if len(list) > 0:
                    return float(list[0].text.split(" ")[0].replace(",", ""))
                return 0.0
        except Exception as e:
            print("Error fetching price from c2c okex!")
            print("err:", e)
            return 0.0

    async def _get_binance_price(self, _type="buy", symbol="USDT"):
        url = "https://p2p.binance.com/zh-CN/trade/{}/{}".format(_type, symbol)
        # print(url)

        try:
            response = await asyncio.wait_for(self.session.get(url), 120)
            # response = await self.session.get(url)
            if response.status_code == 200:
                # print(response.html.html)
                await response.html.arender(sleep=4)
                # await response.html.arender()
                # print(response.html.html)
                soup = BeautifulSoup(response.html.html, 'html.parser')
                # print(soup)
                list = soup.find_all("div", {"class": "css-1m1f8hn"})
                # print(list)
                if len(list) > 0:
                    return float(list[0].text.split(" ")[0].replace(",", ""))
                return 0.0
        except Exception as e:
            print("Error fetching price from c2c binance!")
            print("err:", e)
            return 0.0

    async def get_binance_rate(self):
        buy = await self._get_binance_price(_type="buy")
        sell = await self._get_binance_price(_type="sell")
        price = 0
        if (buy != 0.0 and sell != 0.0):
            price = (buy + sell) / 2
        else:
            price = max(buy, sell)
        if price != 0:
            self.rate['USD']['CNY'] = 1 / price
        # self.rate['CNY']['USD'] = price
        return self.rate

    async def get_okex_rate(self):
        buy = await self._get_okex_price(_type="buy", symbol="usdt")
        sell = await self._get_okex_price(_type="sell", symbol="usdt")
        # print("buy: ", buy)
        # print("sell: ", sell)
        price = 0
        if (buy != 0.0 and sell != 0.0):
            price = (buy + sell) / 2
        else:
            price = max(buy, sell)
        if price != 0:
            self.rate['USD']['CNY'] = 1 / price
        # self.rate['CNY']['USD'] = price
        return self.rate

    async def get_okex_btc_price(self):
        buy = await self._get_okex_price(_type="buy", currency="usdt")
        sell = await self._get_okex_price(_type="sell", currency="usdt")
        # print("buy: ", buy)
        # print("sell: ", sell)
        price = 0
        if (buy != 0.0 and sell != 0.0):
            price = (buy + sell) / 2
        else:
            price = max(buy, sell)
        self.rate["USD"]['BTC'] = price
        return self.rate


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    c2c = C2C()
    # loop.run_until_complete(c2c.get_okex_rate())
    loop.run_until_complete(c2c.get_binance_rate())
    print(c2c.rate)
    # loop.run_forever()
