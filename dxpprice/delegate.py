import asyncio
from dxp.http_rpc import HTTPRPC

from dxpprice.feedapi import FeedApi


class Delegate():
    def __init__(self, config) -> None:
        self.config = config
        self.last_missed = 0
        self.total_missed = 0
        self.init_config()

    def init_config(self):
        self.api = FeedApi(self.config)
        self.interval = int(self.config['delegate']['interval']) * 60
        self.max_miss = int(self.config['delegate']['max_miss'])
        self.keys = self.config['delegate']['keys']

    def get_key(self, key):
        new_key = None
        for k in self.keys:
            if k == key:
                continue
            new_key = k
            break
        return new_key

    async def check_task(self):
        try:
            while True:
                result = self.api.get_blockproducer_info()
                # print(result)
                if result is not None:
                    missed = int(result['total_missed'])
                    if self.total_missed == 0:
                        self.total_missed = missed
                        await asyncio.sleep(self.interval)
                        continue
                    if missed - self.total_missed >= self.max_miss:
                        self.total_missed = missed
                        key = self.get_key(result['signing_key'])
                        self.api.update_blockproducer(self.config['blockproducer'], key)
                await asyncio.sleep(self.interval)
        except Exception as e:
            print("Error: ", e)

    def run_tasks(self, loop):
        return [loop.create_task(self.check_task())]


if __name__ == "__main__":
    import json
    f = open("config.json", encoding='utf-8')
    config = json.load(f)
    loop = asyncio.get_event_loop()
    d = Delegate(config)
    tasks = d.run_tasks(loop)
    loop.run_until_complete(asyncio.wait(tasks))
    loop.run_forever()
