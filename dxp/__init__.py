# -*- coding: utf-8 -*-

from dxp.http_rpc import HTTPRPC  # noqa

from dxp import metadata


__version__ = metadata.version
__author__ = metadata.authors[0]
__license__ = metadata.license
__copyright__ = metadata.copyright
