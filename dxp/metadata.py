# -*- coding: utf-8 -*-
"""Project python-dxp

Information describing the project.
"""

# The package name, which is also the "UNIX name" for the project.
package = 'dxp'
project = "python api for DxpChain"
project_no_spaces = project.replace(' ', '')
version = '0.6.17'
description = 'api for DxpChain'
authors = ['Alt']
authors_string = ', '.join(authors)
emails = ['pch957@gmail.com']
license = 'MIT'
copyright = '2015 ' + authors_string
url = 'https://github.com/pch957/python-dxp'
